# MAYhem Arena

### Kick-off

---slide---

## Doelen

* **FUN**
* Bijschaven kennis & kunde
* Verkennen van nieuwe taal/library
* Testen van format

---slide---

## Hoe dan?

* Kleine code game van 12 tot 28 mei
* iets meer ruimte voor strategy/complexiteit
* bouwen en testen op handig moment voor jezelf

---slide---

### The Game
* real-time (20 updates per seconde)
* statische fights, geen movement of map
* je bepaald de acties van 3 **_Code Heroes_** in een 3vs3
* degene met de langst levende **_Code Hero(es)_** wint
* competitie van 1x iedereen tegen iedereen

---slide---

### The Heroes
* Health
* Power
* Regeneration
* Armor
* Resistance

---slide---

## API

* JSON over TCP/IP (poort 1337)
* 1 bericht per regel
* Library met Java POJO's voor jackson beschikbaar
* Uitgebreide beschrijving in de API repo README

---slide---

## Tips

* Reageer op een status bericht met 0+ acties
* Maak eerst iets stabiels en daarna iets beters ;)
* Kijk wat de skills precies doen en verzin een strategie

---slide---

## Planning

* Tot 21 Mei "easy" bot online
* Practice sessie op 21 Mei 12:00
* 21-28 Mei "medium" bot online
* **Final battle op 28 Mei 12:00**

---slide---

## Lets go

* API: [github.com/Robbert1/mayhem-api](https://github.com/Robbert1/mayhem-api)
* Game: [mayhem.jdriven.com](http://mayhem.jdriven.com)
* Eigen server: [robbert1/mayhem-server](https://hub.docker.com/repository/docker/robbert1/mayhem-server)


