# presentation-template

This is a template repository for making new presentation repositories

## Cheatsheet

### Add a slide

Adding a new slide can be done by adding a line that contains the content: `---slide---`

e.g.:

> ## title 1
> 
> * Content
> 
> ---slide---
> 
> ## title2
> 
> * content


## Hosting

Slides are hosted automatically using gitlab pages.
You can find the url in your repo under `Settings->Pages`

## Printing your slides

add `?print-pdf` to your url. 
e.g. https://jdriven.gitlab.io/training/java-15/?print-pdf
It will now show all the pages in print-view.
Now use the browser's print function with the following settings:

Change the Destination setting to Save as PDF.
Change the Layout to Landscape.
Change the Margins to None.
Enable the Background graphics option.

now press save